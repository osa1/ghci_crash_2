#!/bin/bash

set -e
set -x

which ghc-stage2

rm -f *.o *.hi
ghc-stage2 -O2 A.hs

ghci_script=":load B.hs C.hs
import B
import C
import Data.IORef
import System.Mem
import GHCi.ObjLink
writeIORef ref (Just x)
unloadObj \"/home/omer/haskell/ghci_crash_2/A.o\"
performGC
Just fun <- readIORef ref
fun 123"

echo -e "$ghci_script" | ghc-stage2 --interactive +RTS -DS
