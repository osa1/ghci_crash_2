module A (f) where

{-# NOINLINE f #-}
f :: Int -> Int
f x = x * x * x * x
