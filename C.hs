module C (ref) where

import Data.IORef
import System.IO.Unsafe

{-# NOINLINE ref #-}
ref :: IORef (Maybe (Int -> Int))
ref = unsafePerformIO (newIORef Nothing)
